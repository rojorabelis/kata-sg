
import dto.Account;
import dto.Wallet;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Account account = new Account(new Wallet());
        account.saveAmount("2020-01-01", new BigDecimal(1000));
        account.retrieveAmount("2020-01-02", new BigDecimal(100));
        account.saveAmount("2020-01-03", new BigDecimal(250));
        System.out.println(account.getManagementActs());
        System.out.println(account.getCurrentAmount());
    }

}
