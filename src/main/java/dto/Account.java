package dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;

public class Account {
    private Wallet wallet;
    private BigDecimal currentAmount;

    public Account(Wallet wallet) {
        this.wallet = wallet;
    }

    /***
     *  This method get all list of transaction in the account
     * @return list of transaction
     */
    public Wallet getWallet() {
        return wallet;
    }

    /***
     * Get the amount available on the account
     * @return value of amount
     */
    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    /***
     * This method add in a management acts the amount that would be fetch
     * @param date date of retrieving amount
     * @param amount amount retrieving
     * @return true if correctly add otherwise false
     * @throws IllegalArgumentException if the format of date is not correct
     */
    public boolean retrieveAmount(String date, BigDecimal amount) throws IllegalArgumentException {
        if(date.matches("\\d{4}-\\d{2}-\\d{2}")){
            LocalDate toDate = LocalDate.parse(date);
            addInManagementActs(toDate,amount.negate());
            return true;
        }
        throw new IllegalArgumentException("Invalid date format");
    }

    /***
     * This method add in a management acts the amount that would be save
     * @param date date of deposit amount
     * @param amount amount to save
     * @return true if correctly add otherwise false
     * @throws IllegalArgumentException if the format of date is not correct
     */
    public boolean saveAmount(String date, BigDecimal amount)  throws IllegalArgumentException {
        if(date.matches("\\d{4}-\\d{2}-\\d{2}")){
            LocalDate toDate = LocalDate.parse(date);
            addInManagementActs(toDate,amount);
            return true;
        }
        throw new IllegalArgumentException("Invalid date format");
    }

    private void addInManagementActs(LocalDate date, BigDecimal amount){
        wallet.getTransactionList().add(new Transaction(date, amount));
        currentAmount = wallet.getTransactionList().stream().map(Transaction::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /***
     *
     * @return get in String list of transaction in the account sorting by date
     */
    public String getManagementActs(){
        StringBuilder result = new StringBuilder();
        wallet.getTransactionList().stream().sorted(Comparator.comparing(Transaction::getDate)).forEach((transaction ->result.append("date: ").append(transaction.getDate()).append("\t amount: ").append(transaction.getValue()).append("\n") ));
        return result.toString();
    }

    /***
     *
     * @return get in String the date of today with the amount available on the account
     */
    public String showCurrentAmount(){
        return "Today : "+LocalDate.now()+", your current amount is : "+currentAmount;
    }
}
