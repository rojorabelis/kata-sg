package dto;

import java.util.LinkedList;
import java.util.List;

public class Wallet {
    private List<Transaction> transactionList = new LinkedList<>();

    public List<Transaction> getTransactionList() {
        return transactionList;
    }
}
