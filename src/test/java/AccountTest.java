import dto.Account;
import dto.Wallet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;


@RunWith(MockitoJUnitRunner.class)
public class AccountTest {

	private Account account;

	@Before
	public void initialise() {
		account = new Account(new Wallet());
	}
	
	@Test
	public void should_create_a_transaction_when_save_amount() {
		
		account.saveAmount("2020-01-01", new BigDecimal(1000));
		Assert.assertEquals(account.getWallet().getTransactionList().size(),1);
	}

	@Test
	public void should_give_correct_amount_when_save_and_retrieve_amount() {
		account.saveAmount("2020-01-01", new BigDecimal(1000));
		account.retrieveAmount("2020-01-02", new BigDecimal(100));
		account.saveAmount("2020-01-03", new BigDecimal(250));
		Assert.assertEquals(account.getCurrentAmount(),new BigDecimal(1150));
	}

}
